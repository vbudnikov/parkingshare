﻿using System.Collections.Generic;
using System.Linq;
using ParkingShare.Data.UnitOfWork;
using ParkingShare.Model.Models;

namespace ParkingShare.Service
{
    public interface IGiveOfferService
    {
        IEnumerable<GiveOffer> GetGiveOffers();
        GiveOffer GetGiveOffer(int? id);
        void CreateGiveOffer(GiveOffer giveOffer);
        void SaveGiveOffer();

    }
    public class GiveOfferService : BaseService, IGiveOfferService
    {
        public GiveOfferService(IParkingShareData data) : base(data)
        {
        }

        public IEnumerable<GiveOffer> GetGiveOffers()
        {
            var giveOffers = (from o in Data.GiveOffers.GetAll()
                join p in Data.ParkingSpaces.GetAll() on o.ParkingSpaceId equals p.Id
                select o);
            return giveOffers;
        }

        public GiveOffer GetGiveOffer(int? id)
        {
            var giveOffer = (from o in Data.GiveOffers.GetAll()
                             join p in Data.ParkingSpaces.GetAll() on o.ParkingSpaceId equals p.Id
                             select o).FirstOrDefault(g=>g.Id ==id);
            return giveOffer;
        }

        public void CreateGiveOffer(GiveOffer giveOffer)
        {
            if (Data.ParkingSpaces.Get(space => space.Id == giveOffer.ParkingSpaceId) == null)
            {
                Data.ParkingSpaces.Add(giveOffer.ParkingSpace);
            }
            Data.GiveOffers.Add(giveOffer);
        }

        public void SaveGiveOffer()
        {
            Data.Commit();
        }
    }
}
