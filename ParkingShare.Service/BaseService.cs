﻿using ParkingShare.Data.UnitOfWork;

namespace ParkingShare.Service
{
    public abstract class BaseService
    {
        protected BaseService(IParkingShareData data)
        {
            this.Data = data;
        }

        protected IParkingShareData Data { get; set; }
    }
}
