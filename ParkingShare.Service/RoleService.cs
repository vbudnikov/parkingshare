﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ParkingShare.Data;
using ParkingShare.Data.UnitOfWork;
using ParkingShare.Model.Models;

namespace ParkingShare.Service
{
    public interface IRoleService
    {
        IEnumerable<IdentityRole> GetApplicationUsers();
        void SaveGiveOffer();
    }

    public class RoleService : BaseService, IRoleService
    {
        private static DbContext dbContext = new ParkingShareEntities();
        private static RoleStore<IdentityRole> roleStore = new RoleStore<IdentityRole>(dbContext);
        private static RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(roleStore);

        public RoleService(IParkingShareData data) : base(data)
        {
        }

        public IEnumerable<IdentityRole> GetApplicationUsers()
        {
            return roleManager.Roles;
        }

        public void SaveGiveOffer()
        {
            Data.Commit();
        }
    }
}