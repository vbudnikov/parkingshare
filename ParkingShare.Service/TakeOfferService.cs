﻿using System.Collections.Generic;
using ParkingShare.Data.UnitOfWork;
using ParkingShare.Model.Models;

namespace ParkingShare.Service
{
    public interface ITakeOfferService
    {
        IEnumerable<TakeOffer> GetTakeOffers();
        TakeOffer GetTakeOffer(int id);
        void CreateTakeOffer(TakeOffer takeOffer);
        void SaveTakeOffer();

    }
    public class TakeOfferService : BaseService, ITakeOfferService
    {
        public TakeOfferService(IParkingShareData data) : base(data)
        {
        }

        public IEnumerable<TakeOffer> GetTakeOffers()
        {
            var takeOffers = Data.TakeOffers.GetAll();
            return takeOffers;
        }

        public TakeOffer GetTakeOffer(int id)
        {
            var takeOffer = Data.TakeOffers.GetById(id);
            return takeOffer;
        }

        public void CreateTakeOffer(TakeOffer takeOffer)
        {
            Data.TakeOffers.Add(takeOffer);
        }

        public void SaveTakeOffer()
        {
            Data.Commit();
        }
    }
}
