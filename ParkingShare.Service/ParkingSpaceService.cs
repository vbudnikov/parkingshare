﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParkingShare.Data.UnitOfWork;
using ParkingShare.Model.Models;

namespace ParkingShare.Service
{
    public interface IParkingSpaceService
    {
        IEnumerable<ParkingSpace> GetParkingSpaces();
    }
    public class ParkingSpaceService : BaseService, IParkingSpaceService
    {
        public ParkingSpaceService(IParkingShareData data) : base(data)
        {
        }
        public IEnumerable<ParkingSpace> GetParkingSpaces()
        {
            var parkingSpaces = Data.ParkingSpaces.GetAll();
            return parkingSpaces;
        }
    }
}
