﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParkingShare.Data.UnitOfWork;
using ParkingShare.Model.Models;

namespace ParkingShare.Service
{
    public interface IApplicationUserService
    {
        IEnumerable<ApplicationUser> GetApplicationUsers();
        ApplicationUser GetUser(string id);
        void CreateUser(ApplicationUser user);
        void UpdateUser(ApplicationUser user);


        void SaveUser();
    }
    public class ApplicationUserService : BaseService, IApplicationUserService
    {
        public ApplicationUserService(IParkingShareData data) : base(data)
        {
        }

        public IEnumerable<ApplicationUser> GetApplicationUsers()
        {
            return Data.Users.GetAll();
        }

        public ApplicationUser GetUser(string id)
        {
            return Data.Users.GetById(id);
        }

        public void CreateUser(ApplicationUser user)
        {
            Data.Users.Add(user);
        }

        public void UpdateUser(ApplicationUser user)
        {
            Data.Users.Update(user);
        }

        public void SaveUser()
        {
            Data.Commit();
        }
    }
}
