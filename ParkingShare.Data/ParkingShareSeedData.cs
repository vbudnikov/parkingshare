﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParkingShare.Model.Models;

namespace ParkingShare.Data
{
    public class ParkingShareSeedData : DropCreateDatabaseIfModelChanges<ParkingShareEntities>
    {
        protected override void Seed(ParkingShareEntities context)
        {
            GetTakeOffers().ForEach(t => context.TakeOffers.Add(t));
            GetGiveOffers().ForEach(g => context.GiveOffers.Add(g));

            context.Commit();
        }

        private static List<TakeOffer> GetTakeOffers()
        {
            return new List<TakeOffer>
            {
                new TakeOffer {
                    AcceptableAddress = "Manastirski livadi",
                    AcceptablePrice = 120,
                    NecessetyPeriod = 30
                },
                new TakeOffer {
                    AcceptableAddress = "Izgrev",
                    AcceptablePrice = 50,
                    NecessetyPeriod = 5
                },
                new TakeOffer {
                    AcceptableAddress = "Business Park",
                    AcceptablePrice = 600,
                    NecessetyPeriod = 365
                }
            };
        }

        private static List<GiveOffer> GetGiveOffers()
        {
            return new List<GiveOffer>
            {
                new GiveOffer {
                    ParkingSpace = new ParkingSpace
                    {
                        Address = "Manastirski livadi",
                        Length = 5.00,
                        Width = 2.20
                    },
                    AllowedPeriod = 60,
                    Price = 200
                },
                new GiveOffer {
                    ParkingSpace = new ParkingSpace
                    {
                        Address = "Banishora",
                        Length = 4.50,
                        Width = 2.20
                    },
                    AllowedPeriod = 10,
                    Price = 50
                },
                new GiveOffer {
                    ParkingSpace = new ParkingSpace
                    {
                        Address = "Manastirski livadi",
                        Length = 4.50,
                        Width = 2.40
                    },
                    AllowedPeriod = 5,
                    Price = 40
                },
            };
        }
    }
}
