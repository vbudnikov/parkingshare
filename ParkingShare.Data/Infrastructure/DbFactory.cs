﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingShare.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        ParkingShareEntities dbContext;

        public ParkingShareEntities Init()
        {
            return dbContext ?? (dbContext = new ParkingShareEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
