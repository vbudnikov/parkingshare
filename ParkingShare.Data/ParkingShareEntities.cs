using Microsoft.AspNet.Identity.EntityFramework;
using ParkingShare.Model.Models;

namespace ParkingShare.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ParkingShareEntities : IdentityDbContext<ApplicationUser>
    {
        public ParkingShareEntities()
            : base("name=ParkingShareEntities")
        {
        }
        public static ParkingShareEntities Create()
        {
            return new ParkingShareEntities();
        }
        public virtual DbSet<ParkingSpace> ParkingSpaces { get; set; }
        public virtual DbSet<TakeOffer> TakeOffers { get; set; }
        public virtual DbSet<GiveOffer> GiveOffers { get; set; }
        public virtual void Commit()
        {
            SaveChanges();
        }

    }
}