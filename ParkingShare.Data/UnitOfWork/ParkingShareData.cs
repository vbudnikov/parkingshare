﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ParkingShare.Data.Infrastructure;
using ParkingShare.Data.Repositories;
using ParkingShare.Model.Models;

namespace ParkingShare.Data.UnitOfWork
{
    public class ParkingShareData : IParkingShareData
    {
        private readonly IDbFactory dbFactory;
        private ParkingShareEntities dbContext;
        private IDictionary<Type, object> repositories;
        public ParkingShareData(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
            this.repositories = new Dictionary<Type, object>();
        }
        public ParkingShareEntities DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.Init()); }
        }
        public IRepository<TakeOffer> TakeOffers
        {
            get { return GetRepository<TakeOffer>(); }
        }
        public IRepository<GiveOffer> GiveOffers
        {
            get { return GetRepository<GiveOffer>(); }
        }
        public IRepository<ParkingSpace> ParkingSpaces
        {
            get { return GetRepository<ParkingSpace>(); }
        }
        public IRepository<ApplicationUser> Users
        {
            get { return GetRepository<ApplicationUser>(); }
        }
        public IRepository<IdentityRole> Roles
        {
            get { return GetRepository<IdentityRole>(); }
        }
        public void Commit()
        {
            DbContext.Commit();
        }
        private IRepository<T> GetRepository<T>() where T : class
        {
            var type = typeof(T);
            if (!this.repositories.ContainsKey(type))
            {
                var typeOfRepository = typeof(BaseRepository<T>);

                var repository = Activator.CreateInstance(typeOfRepository, dbFactory);
                this.repositories.Add(type, repository);
            }
            var istrue = this.repositories[type] is IRepository<T>;

            return (IRepository<T>)this.repositories[type];
        }
    }
}
