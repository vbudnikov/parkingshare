﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ParkingShare.Data.Repositories;
using ParkingShare.Model.Models;

namespace ParkingShare.Data.UnitOfWork
{
    public interface IParkingShareData
    {
        IRepository<TakeOffer> TakeOffers { get; }
        IRepository<GiveOffer> GiveOffers { get; }
        IRepository<ParkingSpace> ParkingSpaces { get; }
        IRepository<ApplicationUser> Users { get; }
        IRepository<IdentityRole> Roles { get; }

        void Commit();
    }
}
