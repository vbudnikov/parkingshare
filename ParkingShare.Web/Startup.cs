﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ParkingShare.Web.Startup))]
namespace ParkingShare.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
