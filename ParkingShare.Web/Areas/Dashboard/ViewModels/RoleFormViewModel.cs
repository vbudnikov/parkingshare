﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParkingShare.Web.Areas.Dashboard.ViewModels
{
    public class RoleFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}