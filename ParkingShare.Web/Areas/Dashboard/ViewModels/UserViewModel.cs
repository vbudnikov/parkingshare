﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ParkingShare.Web.Areas.Dashboard.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public IEnumerable<IdentityUserRole> Roles{ get; set; }
        public bool IsAdministrator { get; set; }
    }
}