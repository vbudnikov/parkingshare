﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ParkingShare.Data;
using ParkingShare.Web.Areas.Dashboard.ViewModels;

namespace ParkingShare.Web.Areas.Dashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RolesController : Controller
    {
        private static DbContext dbContext = new ParkingShareEntities();
        private static RoleStore<IdentityRole> roleStore = new RoleStore<IdentityRole>(dbContext);
        private static RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(roleStore);
        // GET: Dashboard/Roles
        public ActionResult Index()
        {
            var roles = roleManager.Roles;
            return View(roles);
        }

        // GET: Dashboard/Roles/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dashboard/Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Roles/Create
        [HttpPost]
        public ActionResult Create(RoleFormViewModel collection)
        {
            try
            {
                var role = roleManager.FindByName(collection.Name);
                if (role == null)
                {
                    role = new IdentityRole(collection.Name);
                    roleManager.Create(role);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Roles/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dashboard/Roles/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Roles/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dashboard/Roles/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
