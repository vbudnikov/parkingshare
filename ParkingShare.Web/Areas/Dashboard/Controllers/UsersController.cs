﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ParkingShare.Data;
using ParkingShare.Model.Models;
using ParkingShare.Service;
using ParkingShare.Web.Areas.Dashboard.ViewModels;
using ParkingShare.Web.Controllers;

namespace ParkingShare.Web.Areas.Dashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UsersController : BaseController
    {
        private readonly ApplicationUserService applicationUserService;

        public UsersController(IMapper mapper, ApplicationUserService applicationUserService) : base(mapper)
        {
            this.applicationUserService = applicationUserService;
        }

        // GET: Dashboard/Dashboard
        public ActionResult Index()
        {
            var users = applicationUserService.GetApplicationUsers();
            var userViewModels = new List<UserViewModel>();
            foreach (var user in users)
            {
                userViewModels.Add(
                new UserViewModel
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Roles = user.Roles
                });
            }

            return View(userViewModels);
        }

        //// GET: Dashboard/Dashboard/Details/5
        //public ActionResult Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ApplicationUser applicationUser = db.ApplicationUsers.Find(id);
        //    if (applicationUser == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(applicationUser);
        //}

        //// GET: Dashboard/Dashboard/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Dashboard/Dashboard/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ApplicationUsers.Add(applicationUser);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(applicationUser);
        //}

        // GET: Dashboard/Dashboard/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = applicationUserService.GetUser(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userViewModel = new UserViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Roles = user.Roles,
                IsAdministrator = false
            };

            return View(userViewModel);
        }

        // POST: Dashboard/Dashboard/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IsAdministrator,UserName")] UserViewModel userViewModel)
        {
            using (var context = new ParkingShareEntities())
            {
                ApplicationUser applicationUser = new ApplicationUser();


                if (ModelState.IsValid)
                {
                    applicationUser = applicationUserService.GetUser(userViewModel.Id);
                    var roleStore = new RoleStore<IdentityRole>(context);

                    var userStore = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(userStore);
                    if (userViewModel.IsAdministrator)
                    {
                        userManager.AddToRole(applicationUser.Id, "Administrator");
                    }
                    else
                    {
                        userManager.RemoveFromRole(applicationUser.Id, "Administrator");
                    }

                    applicationUserService.UpdateUser(applicationUser);
                    applicationUserService.SaveUser();
                    return RedirectToAction("Index");
                    
                }
                userViewModel = new UserViewModel
                {
                    Id = applicationUser.Id,
                    UserName = applicationUser.UserName,
                    Roles = applicationUser.Roles,
                    IsAdministrator = false
                };
            }
            return View(userViewModel);
        }

        //// GET: Dashboard/Dashboard/Delete/5
        //public ActionResult Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ApplicationUser applicationUser = db.ApplicationUsers.Find(id);
        //    if (applicationUser == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(applicationUser);
        //}

        //// POST: Dashboard/Dashboard/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(string id)
        //{
        //    ApplicationUser applicationUser = db.ApplicationUsers.Find(id);
        //    db.ApplicationUsers.Remove(applicationUser);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
