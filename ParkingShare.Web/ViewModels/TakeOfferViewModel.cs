﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingShare.Web.ViewModels
{
    public class TakeOfferViewModel
    {
        [Key]
        public int Id { get; set; }
        [Range(0, 1000000)]
        public decimal AcceptablePrice { get; set; }
        [Range(0, 10000)]
        public int NecessetyPeriod { get; set; }
        public string AcceptableAddress { get; set; }
    }
}