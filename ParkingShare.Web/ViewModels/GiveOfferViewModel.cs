﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ParkingShare.Model.Models;

namespace ParkingShare.Web.ViewModels
{
    public class GiveOfferViewModel
    {
        public int Id { get; set; }
        [Range(1, 1000000)]
        public decimal Price { get; set; }
        [Range(1, 10000)]
        public int AllowedPeriod { get; set; }
        public int ParkingSpaceId { get; set; }
        public ParkingSpace ParkingSpace { get; set; }
    }
}