﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ParkingShare.Model.Models;
using ParkingShare.Service;
using ParkingShare.Web.ViewModels;

namespace ParkingShare.Web.Mappings
{
    public class AutoMapperConfiguration
    {
        public static MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(c =>
            {
                c.AddProfile(new PresentationMappingProfile());
                c.AddProfile(new BLMappingProfile());
            });

            return config;
        }
    }

    public class PresentationMappingProfile : Profile
    {
        public PresentationMappingProfile()
        {
            CreateMap<TakeOffer, TakeOfferViewModel>();
            CreateMap<TakeOfferViewModel, TakeOffer>();

            CreateMap<GiveOffer, GiveOfferViewModel>();
            CreateMap<GiveOfferViewModel, GiveOffer>();
        }
    }
    public class BLMappingProfile : Profile
    {
    }
}