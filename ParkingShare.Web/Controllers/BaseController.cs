﻿using System.Web.Mvc;
using AutoMapper;

namespace ParkingShare.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IMapper mapper;

        public BaseController()
        {

        }
        public BaseController(IMapper mapper)
        {
            this.mapper = mapper;
        }
    }
}