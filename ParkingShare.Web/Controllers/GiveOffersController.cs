﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ParkingShare.Data;
using ParkingShare.Model.Models;
using ParkingShare.Service;
using ParkingShare.Web.ViewModels;

namespace ParkingShare.Web.Controllers
{
    [Authorize(Roles = "Administrator, User")]

    public class GiveOffersController : BaseController
    {
        private readonly GiveOfferService giveOfferService;
        private readonly ParkingSpaceService parkingSpaceService;

        public GiveOffersController(IMapper mapper, GiveOfferService giveOfferService, ParkingSpaceService parkingSpaceService) : base(mapper)
        {
            this.giveOfferService = giveOfferService;
            this.parkingSpaceService = parkingSpaceService;
        }


        // GET: GiveOffer
        public ActionResult Index()
        {
            var giveOffers = giveOfferService.GetGiveOffers();
            var giveOfferViewModels = mapper.Map<IEnumerable<GiveOffer>, IEnumerable<GiveOfferViewModel>>(giveOffers);
            return View(giveOfferViewModels);
        }

        // GET: GiveOffer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var giveOffer = giveOfferService.GetGiveOffer(id);

            var giveOfferViewModel = mapper.Map<GiveOffer, GiveOfferViewModel>(giveOffer);
            if (giveOfferViewModel == null)
            {
                return HttpNotFound();
            }
            return View(giveOfferViewModel);
        }

        // GET: GiveOffer/Create
        public ActionResult Create()
        {
            ViewBag.ParkingSpaceId = new SelectList(parkingSpaceService.GetParkingSpaces(), "Id", "Address");
            return View();
        }

        // POST: GiveOffer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Price,AllowedPeriod,ParkingSpaceId")] GiveOfferViewModel giveOfferViewModel)
        {
            if (ModelState.IsValid)
            {
                var giveOffer = mapper.Map<GiveOfferViewModel, GiveOffer>(giveOfferViewModel);
                giveOfferService.CreateGiveOffer(giveOffer);
                giveOfferService.SaveGiveOffer();
                return RedirectToAction("Index");
            }

            ViewBag.ParkingSpaceId = new SelectList(parkingSpaceService.GetParkingSpaces(), "Id", "Address", giveOfferViewModel.ParkingSpaceId);
            return View(giveOfferViewModel);
        }

        //// GET: GiveOffer/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GiveOfferViewModel giveOfferViewModel = db.GiveOfferViewModels.Find(id);
        //    if (giveOfferViewModel == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ParkingSpaceId = new SelectList(db.ParkingSpaces, "Id", "Address", giveOfferViewModel.ParkingSpaceId);
        //    return View(giveOfferViewModel);
        //}

        //// POST: GiveOffer/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Price,AllowedPeriod,ParkingSpaceId")] GiveOfferViewModel giveOfferViewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(giveOfferViewModel).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ParkingSpaceId = new SelectList(db.ParkingSpaces, "Id", "Address", giveOfferViewModel.ParkingSpaceId);
        //    return View(giveOfferViewModel);
        //}

        //// GET: GiveOffer/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GiveOfferViewModel giveOfferViewModel = db.GiveOfferViewModels.Find(id);
        //    if (giveOfferViewModel == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(giveOfferViewModel);
        //}

        //// POST: GiveOffer/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    GiveOfferViewModel giveOfferViewModel = db.GiveOfferViewModels.Find(id);
        //    db.GiveOfferViewModels.Remove(giveOfferViewModel);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
