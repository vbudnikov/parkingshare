﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ParkingShare.Model.Models;
using ParkingShare.Service;
using ParkingShare.Web.ViewModels;

namespace ParkingShare.Web.Controllers
{
    [Authorize(Roles = "Administrator, User")]

    public class TakeOffersController : BaseController
    {
        private readonly TakeOfferService takeOfferService;
        public TakeOffersController(IMapper mapper, TakeOfferService takeOfferService) : base(mapper)
        {
            this.takeOfferService = takeOfferService;
        }
        // GET: TakeOffers
        public ActionResult Index()
        {
            var takeOffers = takeOfferService.GetTakeOffers();
            var takeOfferViewModels = mapper.Map<IEnumerable<TakeOffer>, IEnumerable<TakeOfferViewModel>>(takeOffers);
            return View(takeOfferViewModels);
        }

        // GET: TakeOffers/Details/5
        public ActionResult Details(int id)
        {
            var takeOffer = takeOfferService.GetTakeOffer(id);
            if (takeOffer != null)
            {
                var takeOfferViewModel = mapper.Map<TakeOffer, TakeOfferViewModel>(takeOffer);
                return View(takeOfferViewModel);
            }
           return RedirectToAction("Index");
        }

        // GET: TakeOffers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TakeOffers/Create
        [HttpPost]
        public ActionResult Create(TakeOfferViewModel collection)
        {
            try
            {
                var takeOffer = mapper.Map<TakeOfferViewModel, TakeOffer>(collection);

                    takeOfferService.CreateTakeOffer(takeOffer);

                    takeOfferService.SaveTakeOffer();


                return RedirectToAction("Index");
            }
            catch 
            {
                return View();
            }
        }

        // GET: TakeOffers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TakeOffers/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TakeOffers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TakeOffers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
