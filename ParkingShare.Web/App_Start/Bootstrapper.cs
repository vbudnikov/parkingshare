﻿using ParkingShare.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using ParkingShare.Data.Repositories;
using ParkingShare.Data.UnitOfWork;
using ParkingShare.Service;
using ParkingShare.Web.Mappings;

namespace ParkingShare.Web
{
    public class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainer();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<ParkingShareData>().As<IParkingShareData>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
            builder.RegisterType<TakeOfferService>().As<TakeOfferService>();
            builder.RegisterType<GiveOfferService>().As<GiveOfferService>();
            builder.RegisterType<ParkingSpaceService>().As<ParkingSpaceService>();
            builder.RegisterType<ApplicationUserService>().As<ApplicationUserService>();




            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .AsClosedTypesOf(typeof(BaseRepository<>));
            //// Services
            //builder.RegisterAssemblyTypes(typeof(TakeOfferService).Assembly)
            //   .Where(t => t.Name.EndsWith("Service"))
            //   .AsImplementedInterfaces().InstancePerRequest();

            var mapper = AutoMapperConfiguration.Configure().CreateMapper();
            builder.RegisterInstance<IMapper>(mapper);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}