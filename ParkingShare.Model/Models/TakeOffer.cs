﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingShare.Model.Models
{
    public class TakeOffer
    {
        public int Id { get; set; }
        public decimal AcceptablePrice { get; set; }
        public int NecessetyPeriod { get; set; }
        public string AcceptableAddress { get; set; }
    }
}
