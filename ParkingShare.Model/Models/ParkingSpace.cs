﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingShare.Model.Models
{
    public class ParkingSpace
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
    }
}
