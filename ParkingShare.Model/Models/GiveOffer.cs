﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingShare.Model.Models
{
    public class GiveOffer
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int AllowedPeriod { get; set; }
        public int ParkingSpaceId { get; set; }
        public ParkingSpace ParkingSpace { get; set; }
    }
}
